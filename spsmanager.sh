#!/bin/sh

# looks for file $1 in folder $2 from bottom up
# REFERENCE: https://unix.stackexchange.com/a/22215
_find_up() {
  _find_up_filename=$1
  _find_up_path=$2
  while [ "$_find_up_path" != "" ] && [ ! -f "$_find_up_path/$_find_up_filename" ]; do
    _find_up_path=${_find_up_path%/*}
  done
  echo "$_find_up_path"
}

# helps find real path to script file
# REFERENCE: https://stackoverflow.com/a/29835459
_rreadlink() ( # Execute the function in a *subshell* to localize variables and the effect of `cd`.

  target=$1 fname='' targetDir='' CDPATH=''

  # Try to make the execution environment as predictable as possible:
  # All commands below are invoked via `command`, so we must make sure that `command`
  # itself is not redefined as an alias or shell function.
  # (Note that command is too inconsistent across shells, so we don't use it.)
  # `command` is a *builtin* in bash, dash, ksh, zsh, and some platforms do not even have
  # an external utility version of it (e.g, Ubuntu).
  # `command` bypasses aliases and shell functions and also finds builtins
  # in bash, dash, and ksh. In zsh, option POSIX_BUILTINS must be turned on for that
  # to happen.
  { \unalias command; \unset -f command; } >/dev/null 2>&1
  # shellcheck disable=SC2034
  [ -n "$ZSH_VERSION" ] && options[POSIX_BUILTINS]=on # make zsh find *builtins* with `command` too.

  while :; do # Resolve potential symlinks until the ultimate target is found.
    [ -L "$target" ] || [ -e "$target" ] || { command printf '%s\n' "ERROR: '$target' does not exist." >&2; return 1; }
    command cd "$(command dirname -- "$target")" || exit 1 # Change to target dir; necessary for correct resolution of target path.
    fname=$(command basename -- "$target") # Extract filename.
    [ "$fname" = '/' ] && fname='' # !! curiously, `basename /` returns '/'
    if [ -L "$fname" ]; then
      # Extract [next] target path, which may be defined
      # *relative* to the symlink's own directory.
      # Note: We parse `ls -l` output to find the symlink target
      #       which is the only POSIX-compliant, albeit somewhat fragile, way.
      target=$(command ls -l "$fname")
      target=${target#* -> }
      continue # Resolve [next] symlink target.
    fi
    break # Ultimate target reached.
  done
  targetDir=$(command pwd -P) # Get canonical dir. path
  # Output the ultimate target's canonical path.
  # Note that we manually resolve paths ending in /. and /.. to make sure we have a normalized path.
  if [ "$fname" = '.' ]; then
    command printf '%s\n' "${targetDir%/}"
  elif  [ "$fname" = '..' ]; then
    # Caveat: something like /var/.. will resolve to /private (assuming /var@ -> /private/var), i.e. the '..' is applied
    # AFTER canonicalization.
    command printf '%s\n' "$(command dirname -- "${targetDir}")"
  else
    command printf '%s\n' "${targetDir%/}/$fname"
  fi
)

cd "$(dirname -- "$(_rreadlink "$0")")" || exit 1

# prompt user to select store
echo "Choose password store:"
_store=$(fzf < "stores.txt" --height 40%)
[ "$_store" ] || exit 1
echo "$_store"
echo ""

# prompt user to select directory inside the store
echo "Choose subdirectory(if needed):"
_dir=$(find "$_store" -type d -not -path '*/\.git*' \
    | sed -e "s#^$_store/##" \
  | fzf --height 40%)
[ "$_dir" ] || exit 1
echo "$_dir"
echo ""

if [ "$_store" = "$_dir" ]; then
  _gpg_id_path=$_dir/.gpg-id
else
  _gpg_id_path=$_store/$_dir/.gpg-id
fi

if [ -f "$_gpg_id_path" ]; then
  echo "Selected folder is encrypted for:"
  cat "$_gpg_id_path"

  echo ""
  echo "What action would you like to take?"
  echo "  1) revoke access"
  echo "  2) give access"
  echo "  3) deinit password store in specified folder"
  until expr "$_action" : '^[1-3]$' > /dev/null; do
    printf "Action [1-3]: "
    read -r _action
    if [ "$_action" = 1 ]; then
      # promt user to select keys to revoke access for
      _keys_to_remove=$(fzf < "$_gpg_id_path" -m --height 40%)
      [ "$_keys_to_remove" ] || exit 1

      # new_keys = old_keys - keys_to_remove
      _tmp="$(mktemp)"
      sort "$_gpg_id_path" > "$_tmp"
      _new_keys=$(echo "$_keys_to_remove" \
          | sort \
          | comm -23 "$_tmp" - \
        | xargs)
    elif [ "$_action" = 2 ]; then
      # prompt user to select keys to give access for
      _keys_to_add=$(gpg -k \
          | grep ultimate \
          | cut -b 26- \
          | grep -v "$(xargs <"$_gpg_id_path" | sed -E 's/\ /\\|/g' | sed 's/\n//')"\
        | fzf -m --height 40%)
      [ "$_keys_to_add" ] || exit 1

      # get emails only
      _keys_to_add=$(echo "$_keys_to_add" | sed -E 's/.*<(.+)>.*/\1/')

      # new_keys = old_keys + keys_to_add
      _new_keys=$(echo "$_keys_to_add" \
          | cat "$_gpg_id_path" - \
          | xargs \
          | sort \
        | uniq)
    elif [ "$_action" = 3 ]; then
      # TODO do something smart here instead of just exiting
      if [ "$_store" = "$_dir" ]; then
        echo "You probably should not deinit the entire store. Exiting..."
        exit 1
      fi

      # new_keys is empty string
      _new_keys="\"\""
    fi
  done

  # build pass command for selected store with new keys
  _pass_cmd="PASSWORD_STORE_DIR=$_store pass init $_new_keys"
  if [ "$_store" != "$_dir" ]; then
    _pass_cmd="${_pass_cmd} -p $_dir"
  fi

else
  echo "Selected folder does not have a .gpg-id file"

  # find nearest .gpg-id file
  _gpg_id_dir=$(_find_up ".gpg-id" "$_store/$_dir")
  _gpg_id_path="$_gpg_id_dir"/.gpg-id
  echo "Found .gpg-id in $_gpg_id_dir"
  echo "Selected folder is encrypted for:"
  cat "$_gpg_id_path"
  echo ""

  until expr "$_init" : '^[y|n]$' > /dev/null; do
    printf "Do you want to init password store in selected subdirectory with parent store's keys? [y/n]: "
    read -r _init
    if [ "$_init" = "n" ]; then
      exit
    fi
  done

  _pass_cmd="PASSWORD_STORE_DIR=$_store pass init $(xargs < "$_gpg_id_path") -p $_dir"

fi

echo ""
echo "About to run:"
echo "$_pass_cmd"
echo ""
until expr "$_continue" : '^[y|n]$' > /dev/null; do
  printf "Continue? [y/n]: "
  read -r  _continue
  if [ "$_continue" = 'n' ]; then
    exit 1
  fi
done

echo ""
eval "$_pass_cmd"
