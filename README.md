# Shared password store manager (for [zx2c4 `pass`](https://www.passwordstore.org/))

An interactive POSIX-complaint shell script that helps you generate a `pass init` command for any password store and/or store's subdirectory on your system.

If you share a password store with your team and are looking for a way to manage subfolder restrictions in a way that does not involve manually editing `.gpg-id` or handcrafing commands to reinit password stores in subfolder, this tool is for you.

## Features

- [x] revoke access
- [x] give access to any number of owners of the keys you trust `gpg -k | grep ultimate`
- [x] deinit password store in selected subfolder
- [x] init a new password store in selected subfolder

## Requirements

- [fzf](https://github.com/junegunn/fzf)
- any other dependencies required for normal `pass` usage

## Installation

1. Clone the repo
2. `cd shared-password-store-manager`
3. Add shared password store path to `stores.txt` file `echo /home/user/.pass/teamname >> stores.txt`. Repeat for any number of stores you have.
4. Create a symlink to `spsmanager.sh` in `/usr/local/bin` or any other folder in your `PATH`. `cd /usr/local/bin` `sudo ln -s ~/path/to/shared-password-store-manager/spspanager.sh spsmanager`
